
import numpy as np
a = np.loadtxt('timevsnodes.txt')
import matplotlib.pyplot as plt
plt.plot(a[6], a[0], 'r*', markersize = 14, label = "Maps")
plt.plot(a[7], a[1], 'r*', markersize = 14)
plt.plot(a[6],a[2], 'go', markersize = 10, label = "reduces")
plt.plot(a[7],a[3], 'go', markersize = 10,)
plt.plot(a[6],a[4], 'yd', markersize = 10, label = "Total CPU time")
plt.plot(a[7],a[5], 'yd', markersize = 10)
plt.xlim([0, 5])
plt.xlabel("Number of nodes")
plt.ylabel("Time(ms)")
plt.title('Nodes vs Time')
plt.legend(loc = 'Center')
plt.savefig("Nodes vs Time")
plt.show()

